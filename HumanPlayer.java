package Quoridor;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Provides an interface and reads input from System.in to allow a human player to play the game via the console.
 */
public class HumanPlayer extends AbstractPlayer {
	
	private String readWord() {
		return InputReader.readWord();
	}
	
	/**
     * Inputs an instruction from the human player and acts accordingly.
     */
	public Move generateMove (BoardState board, PlayerState playerState, PlayerState otherState) {
		
		List<Move> redoStack = null;
		
		while(true) {
			System.out.print("> ");
			String s = readWord();
						
			if (s.toLowerCase().equals("display") || s.toLowerCase().equals("d")) {
				System.out.println();
				board.displayBoard();
				continue;
			} else if (s.toLowerCase().equals("moves") || s.toLowerCase().equals("m")) {
				System.out.print("Moves:");
				for(PawnMove m : board.possibleMoves(playerState)) {
					System.out.print(' ');
					System.out.print(m);
				}
				System.out.println();
				continue;
			} else if (s.toLowerCase().equals("help") || s.toLowerCase().equals("h")) {
				System.out.println("Nhập vào dòng lệnh:");
				System.out.println(" m[oves] - hiển thị các bước có thể đi");
				System.out.println(" d[isplay] - hiển thị trạng thái của bảng");
				System.out.println(" Lệnh di chuyển: left, right, up or down");
				System.out.println(" Lệnh di chuyển: dưới dạng [a-i][1-9]");
				System.out.println(" Lệnh đặt tường trong biểu mẫu [a-h][1-8][vh]");
				System.out.println(" h[elp] - hiển thị thông báo trợ giúp");
				System.out.println("");
				continue;
			} else if (s.toLowerCase().equals("save") || s.toLowerCase().equals("s")) {
				String fn = readWord();
				
				try {
					FileWriter f = new FileWriter(fn);
					if (playerState.playerNum() == 1) {
						f.write(playerState.getPlayerInitString());
						f.write('\n');
					}
					f.write(otherState.getPlayerInitString());
					f.write('\n');
					if (playerState.playerNum() == 2) {
						f.write(playerState.getPlayerInitString());
						f.write('\n');
					}
					board.save(f);
					f.close();
				} catch (IOException e) {
					System.err.format("Error: failed to save to file '%s'\n", fn);
				}
				continue;
				
			} else if (s.toLowerCase().equals("undo") || s.toLowerCase().equals("u")) {
				if (board.numMoves() >= 2) {
					
					if (redoStack == null) {
						redoStack = new LinkedList<Move>();
					}
					redoStack.addAll(board.undoMove(playerState, otherState));
				} else {
					System.out.println("Can't undo!");
				}
				continue;
			} else if (s.toLowerCase().equals("redo") || s.toLowerCase().equals("r")) {
				if (redoStack != null && redoStack.size() >= 2) {
					for(int i = 0; i < 2; ++i) {
						board.applyMove(redoStack.remove(redoStack.size()-1), i == 0 ? playerState : otherState);
					}
				} else {
					System.out.println("Can't redo!");
				}
				continue;
			} else if (s.toLowerCase().equals("left")) {
				PawnMove pp = board.playerPosition(playerState.playerNum());
				if (board.isMoveValid(pp.left(), playerState)) {
					return pp.left();
				}
			} else if (s.toLowerCase().equals("right")) {
				PawnMove pp = board.playerPosition(playerState.playerNum());
				if (board.isMoveValid(pp.right(), playerState)) {
					return pp.right();
				}
			} else if (s.toLowerCase().equals("up")) {
				PawnMove pp = board.playerPosition(playerState.playerNum());
				if (board.isMoveValid(pp.top(), playerState)) {
					return pp.top();
				}
			} else if (s.toLowerCase().equals("down")) {
				PawnMove pp = board.playerPosition(playerState.playerNum());
				if (board.isMoveValid(pp.bottom(), playerState)) {
					return pp.bottom();
				}
			} else {
				
				if (s.length() >= 3 && new WallMove(s) != null) {
					if (board.isMoveValid(new WallMove(s), playerState)) {
						return new WallMove(s);
					} else if (playerState.wallsLeft() < 1) {
						System.out.println("Không còn tường!");
						continue;
					} else {
						System.out.println("Không thể đặt tường ở đó!");
						continue;
					}
				} else if (s.length() >= 2 && new PawnMove(s) != null) {
					if (board.isMoveValid(new PawnMove(s), playerState)) {
						return new PawnMove(s);
					} else {
						System.out.println("Không thể di chuyển ở đó!");
						continue;
					}
				}
			}
			System.out.format("Lỗi: '%s' bước di chuyển không hợp lệ\n", s);
			System.out.println("Gõ [h]elp để hiện thị danh sách lệnh");
		}
	}
	
	/**
	 * Returns the name of the player
	 * @return String
	 */
	public String getName() {
		return "human";
	}
}