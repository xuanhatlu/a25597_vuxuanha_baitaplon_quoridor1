package Quoridor;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
  * Mô tả trạng thái của bảng
  * Lớp này cung cấp các chức năng chính cho trò chơi.
  * Nó có trách nhiệm tạo, thay đổi, truy vấn và
  * hiển thị trạng thái bảng.
 */
public class BoardState {
	private List <Move> moveStack = new LinkedList<Move>();
	
	private boolean[][] rowConnections = new boolean[9][8];	//mảng 2 chiều giữ cho hai vị trí lưới "trên / dưới" nhau không có một bức tường ở giữa
	private boolean[][] colConnections = new boolean[8][9]; //mảng 2 chiều giữ cho hai vị trí lưới "trên / dưới" nhau không có một bức tường ở giữa
	
	/**
     * Class Constructor. 
     * 
     * Tạo BoardState trong trạng thái mặc định - với người chơi 1 ở vị trí e9 và người chơi 2 tại vị trí e1.
     */
	public BoardState() {
		moveStack.add(new PawnMove("e9"));	//Khởi tạo player 1 bắt đầu ở vị trí e9
		moveStack.add(new PawnMove("e1"));	//Khởi tạo player 2 bắt đầu ở vị trí e1
		
		for(int i = 0; i < 8; ++i) {
			for(int j = 0; j < 9; ++j) {
				rowConnections[j][i] = true;	//Ban đầu không có bức tường, do đó, tất cả các vị trí lưới đều được kết nối
				colConnections[i][j] = true;
			}
		}
	}
	
	/**
     * Lưu lại trạng thái của bảng (hiện tại)
     * 
     * @return 1 bản sao của bảng.
     * 
     * Cho phép thuật toán AI giữ trạng thái trước đó được lưu trữ trong khi
     * tìm kiếm bước di chuyển tốt nhất.
     */
	public BoardState clone() {
		BoardState b = new BoardState();
		for(int n = 2; n < moveStack.size(); ++n) {
			b.moveStack.add(moveStack.get(n));
		}
		for(int i = 0; i < 8; ++i) {
			for(int j = 0; j < 9; ++j) {
				b.rowConnections[j][i] = rowConnections[j][i];	//Ban đầu không có bức tường, do đó, tất cả các vị trí lưới đều được kết nối
				b.colConnections[i][j] = colConnections[i][j];	
			}
		}
		return b;
	}
	
	/**
	 * Trả về vị trí hiện tại của người chơi
	 * 
	 * @param playerNum Nhận diện là của người chơi (1 hay 2)
	 * @return A PawnMove Đại diện cho vị trí của người chơi
	 */
	public PawnMove playerPosition(int playerNum) {
		int i = moveStack.size() - ((moveStack.size() % 2 == playerNum - 1) ? 2 : 1);
		while(moveStack.get(i).toPawnMove() == null) {
			i -= 2;
		}
		return moveStack.get(i).toPawnMove();
	}
	
	
	// In bảng - Ban đầu
	private void displayCell(int row, int col) {
		System.out.print(' ');
		PawnMove p1pos = playerPosition(1); 
		PawnMove p2pos = playerPosition(2);
		if (p1pos.col == col && p1pos.row == row) {
			System.out.print('1');
		} else if (p2pos.col == col && p2pos.row == row) {
			System.out.print('2');
		} else {
			System.out.print(' ');
		}
		System.out.print(' ');
	}

	// In bảng - Tường Dọc
	private void displayVWall(int row, int col) {
		if (col == 8) return;
		if (rowConnections[row][col]) {
			System.out.print('|');
		} else {
			System.out.print('#');
		}
	}

	// In bảng - Hàng
	private void displayRow(int row) {
		System.out.print(row+1);
		System.out.print("  ");
		for(int col = 0; col < 9; ++col) {
			displayCell(row, col);
			displayVWall(row, col);
		}
		System.out.print("  ");
		System.out.print(row+1);
		System.out.println();
	}

	// In bảng - Tường Ngang
	private void displayHWall(int row, int col) {
		if (colConnections[row][col]) {
			System.out.print("---");
		} else {
			System.out.print("###");
		}
		if (col < 8) {
			System.out.print('+');
		}
	}

	// 
	private void displayHWall(int row) {
		if (row == 8) return;
		System.out.print("   ");
		for(int i = 0; i < 9; ++i) {
			displayHWall(row, i);
		}
		System.out.println();
	}
	// In bảng
	public void displayBoard() {

		final String topRow = "    A   B   C   D   E   F   G   H   I ";	
		System.out.println(topRow);
		System.out.println();
		
		for(int row = 0; row < 9; ++row) {
			displayRow(row);
			displayHWall(row);
		}
		
		System.out.println();
		System.out.println(topRow);
	}
	
	/**
     * Kiểm tra tường giữa hai vị trí của bảng
     * @param m1 Vị trí 1
     * @param m2 Vị trí 2
     * @return true if wall, else false
     */
	private boolean wallBetween(PawnMove m1, PawnMove m2) {
		if (m1.col - m2.col != 0) {
			// Nếu là tường - dọc
			return !rowConnections[m1.row][Math.min(m1.col, m2.col)];
		} else {
			return !colConnections[Math.min(m1.row, m2.row)][m1.col];
		}
	}
	
	/**
     * Kiểm tra nhảy
     * @param l liệt kê các di chuyển mà người chơi đã thực hiện
     * @param pp previous position of pawn
     * @param m next position of pawn
     */
	@SuppressWarnings(value = "unchecked")
        //Kiểm tra di chuyển với người chơi khi bị cản trở
	private <T extends Move> void checkMoveForOtherPlayer(List<T> l, PawnMove pp, PawnMove op, PawnMove m) {	
		if (wallBetween(pp, m)) {
			//Nếu có một bức tường giữa thì không làm gì
		} else if (m.equals(op)) {						//Nếu người chơi khác
			PawnMove leap = pp.leap(m);				//Kiểm tra nhảy với người chơi khác
			if ( !leap.insideBoard() || wallBetween(op, leap)) {	//Kiểm tra các bức tường cản trở bước nhảy
					
				leap = pp.leapLeft(m);								//Tạo bước nhảy - trái
				if (leap.insideBoard() && !wallBetween(op, leap)) {	//Kiểm tra xem bước nhảy còn lại bên trong bảng và không có bức tường giữa nó và cầu thủ đối lập
					l.add((T)leap);									//Therefore is a left leap
				}
				leap = pp.leapRight(m);								//Tạo bước nhảy - phải
				if (leap.insideBoard() && !wallBetween(op, leap)) {	//Same as left for right
					l.add((T)leap);									//Therefore is a right leap
				}			
			} else {
				l.add((T)leap);				//Không có bước nhảy vọt, không có tường chắn, do đó là một bước nhảy thẳng
			}
		} else {				
			l.add((T)m);
		}
	}
	
	/**
	 * @param thisPlayer người chơi truy vấn
	 * @param moves danh sách thêm vào
	 */
	private <T extends Move> void generatePossibleMoves(PlayerState thisPlayer, List<T> moves) {
		PawnMove pp = playerPosition(thisPlayer.playerNum()); //Trả về vị trí hiện tại của người chơi (có -2 từ cuối stack do 2 người chơi)
		PawnMove op = playerPosition(thisPlayer.playerNum() % 2 + 1);
		//Checking for players at position of move
		if (pp.row < 8) checkMoveForOtherPlayer(moves, pp, op, pp.bottom());	//If piece has grid below check for moves below
		if (pp.row > 0) checkMoveForOtherPlayer(moves, pp, op, pp.top());	//If piece has grid above check for moves above
		if (pp.col > 0) checkMoveForOtherPlayer(moves, pp, op, pp.left());	//If piece has grid on left check for left moves
		if (pp.col < 8) checkMoveForOtherPlayer(moves, pp, op, pp.right());	//If piece has grid on right check for right moves
		assert(moves.size() != 0);
	}
	
	/** 
     * Tạo ra các bước di chuyển có thể mà một người chơi có thể thực hiện.
     * @param thisplayer The PlayerState class representing the player.
     * @return The List of possible moves.
     */
	public List<PawnMove> possibleMoves(PlayerState thisPlayer) {
		List<PawnMove> moves = new LinkedList<PawnMove>();	//Stack of moves
		
		generatePossibleMoves(thisPlayer, moves);
		
		return moves;
	}
	
	private static ArrayList<WallMove> allWalls = null;
	
	/**
	 * Internal method that fills a list with all possible wall placements
	 */
	@SuppressWarnings(value = "unchecked")
	private <T extends Move> void generatePossibleWallMoves(PlayerState ps, List<T> moves) {
		if (allWalls == null) {
			allWalls = new ArrayList<WallMove>(8*8*2);
			for (int i = 0; i < 8; ++i) {
				for (int j = 0; j < 8; ++j) {
					allWalls.add(new WallMove(i, j, false));
					allWalls.add(new WallMove(i, j, true));
				}
			}
		}

		for (WallMove m : allWalls) {
			if (isMoveValid(m, ps)) {
				moves.add((T)m);
			}
		}
	}

	/**
     * Tính tất cả các bước di chuyển trên tường mà người chơi có thể thực hiện.
     * @param player Object representing the player.
     * @return Một danh sách tất cả các di chuyển tường có thể một người chơi có thể thực hiện.
     */
	public List<WallMove> possibleWallMoves(PlayerState player) {
		
		
		ArrayList<WallMove> moves = new ArrayList<WallMove>(8*8*2);
		
		generatePossibleWallMoves(player, moves);
		
		return moves;
	}
	
	
	/**
     * Generates all pawn and wall moves a player can do.
     * @param player Object representing the player.
     * @return A list of all possible pawn and wall moves.
     */
	public List<Move> allPossibleMoves(PlayerState player) {
		List<Move> moves = new ArrayList<Move>(8*8*2+4);
		generatePossibleMoves(player, moves);
		generatePossibleWallMoves(player, moves);
		return moves;
	}
	
	 /**
     * Determines if a given move is valid.
     * @param m Move to test.
     * @return true if it is a valid move, else false
     */
	public boolean isMoveValid(Move m, PlayerState ps) {
		
		if (m == null) {
			return false;
		}
		if (m.toWallMove() != null) {
			
			if (ps.wallsLeft() <= 0) {
				return false;
			}
			
			WallMove wm = (WallMove)m;
			int col = wm.getCol();
			int row = wm.getRow();
			
			if (col < 0 || row < 0) return false;
			if (col >= 8 || row >= 8) return false;
			
			if (wm.getVertical()) {
				//if (row >= 8) return false;
				
				if (!rowConnections[row][col]) return false;
				if (!rowConnections[row+1][col]) return false;
				
				if (!colConnections[row][col] && !colConnections[row][col+1]) return false;
			} else {
				//if (col >= 8) return false;
				
				if (!colConnections[row][col]) return false;
				if (!colConnections[row][col+1]) return false;
				
				if (!rowConnections[row][col] && !rowConnections[row+1][col]) return false;
			}
			
			if (wm.getVertical()) {
				rowConnections[row][col] = false;
				rowConnections[row+1][col] = false;
			} else {
				colConnections[row][col] = false;
				colConnections[row][col+1] = false;
			}
			boolean hasPath = shortestWinningPath(1) != null &&
							  shortestWinningPath(2) != null;
			if (wm.getVertical()) {
				rowConnections[row][col] = true;
				rowConnections[row+1][col] = true;
			} else {
				colConnections[row][col] = true;
				colConnections[row][col+1] = true;
			}
					
			return hasPath;
		}
		
		for(PawnMove n : possibleMoves(ps)) {	//Cycle through all possible moves until a move is found matching input
			if (n.equals((PawnMove)m)) {	//Otherwise move is not valid so function returns false
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Applies a move without checking for validity.
	 * @param m The move to apply
	 * @param ps The player who is attempting the move
	 */
	public void applyMoveUnchecked(Move m, PlayerState ps) {
		if (m instanceof WallMove) {
			WallMove wm = (WallMove)m;
			int col = wm.getCol();
			int row = wm.getRow();
			if (wm.getVertical()) {
				rowConnections[row][col] = false;
				rowConnections[row+1][col] = false;
			} else {
				colConnections[row][col] = false;
				colConnections[row][col+1] = false;
			}
			ps.useWall();
		}
		
		moveStack.add(m);	//Move is valid so adds to stack and moves piece
	}
	
	/**
	 * Applies a move to the board.
	 * @param m The move to apply
	 * @param ps The player who is attempting the move
	 */
	public void applyMove(Move m, PlayerState ps) {	//Apply move from user input
		if(!isMoveValid(m, ps)) {	//Ensure move is valid; this assert must be replaced, need to work with wrong moves by asking for input again
			System.err.println("\nInternal error: invalid move passed to applyMove");
			System.err.println(m);
			System.err.println(this.playerPosition(0));
			isMoveValid(m, ps);
			int x = 0;
			x = 1 / x;
		}
		applyMoveUnchecked(m, ps);
	}
	
	/**
     * Checks whether the game is over.
     * @return true if we have a winner, else false
     */
	public boolean isOver() {
		return getWinner() != 0;
	}
	
	/**
     * Returns the winner of the game.
     * @return 1 or 2 to indicate the winning player, or 0 to indicate a game in progress
     */
	public int getWinner() {
		if (!playerPosition(1).top().insideBoard()) {
			return 1;
		} else if (!playerPosition(2).bottom().insideBoard()) {
			return 2;
		} else {
			return 0;
		}
	}
	
	/**
     * Tìm đường đi ngắn nhất từ vị trí của người chơi tới cạnh của bảng.
     * @param player player number
     * @return an array of moves
     */
	public PawnMove[] shortestWinningPath(int player) {
		
		PawnMove pp = playerPosition(player);
		int winRow = player == 1 ? 0 : 8;   // dòng để wins
		
		
		int[][] cost = new int[9][9];
		PawnMove[][] parent = new PawnMove[9][9];
		boolean[][] visited = new boolean[9][9];
		
		for(int j = 0; j < 9; ++j) {
			for(int i = 0; i < 9; ++i) {
				cost[j][i] = Integer.MAX_VALUE;
				parent[j][i] = null;
				visited[j][i] = false;
			}
		}
		
		cost[pp.getRow()][pp.getCol()] = 0;
		visited[pp.getRow()][pp.getCol()] = true;
		
		
		PawnMove current = pp;
		do {
			
			
			PawnMove[] nes = { current.left(), current.right(), current.top(), current.bottom() };
			int currcost = cost[current.getRow()][current.getCol()];

			for(PawnMove ne : nes) {
				if (ne.insideBoard() && !wallBetween(current, ne)) {
					if (currcost + 1 < cost[ne.getRow()][ne.getCol()]) {
						cost[ne.getRow()][ne.getCol()] = currcost + 1;
						parent[ne.getRow()][ne.getCol()] = current;
					}
				}
			}

			int lowest = Integer.MAX_VALUE;
			current = null;
			for(int r = 0; r < 9; ++r) {
				for(int c = 0; c < 9; ++c) {
				
					if (!visited[r][c] && cost[r][c] < lowest) {
						lowest = cost[r][c];
						current = new PawnMove(r, c);
					}
				}
			}
			if (current == null) {
				return null;
			}
			visited[current.getRow()][current.getCol()] = true;

		} while (current.getRow() != winRow);
		
		List<PawnMove> res = new LinkedList<PawnMove>();
		while(!parent[current.getRow()][current.getCol()].equals(pp)) {
			res.add(current);
			current = parent[current.getRow()][current.getCol()];
		}
		res.add(current);
		PawnMove[] r = new PawnMove[res.size()];
		return res.toArray(r);
	}
	
	 /**
     * Nhận được ước tính của những người sẽ giành chiến thắng bằng cách phân tích trạng thái của người chơi hiện tại
     * @param currPlayer player state of current player
     * @param oppPlayer  player state of opposite player
     * @return số dương lớn nếu người chơi hiện tại có đường dẫn tới chiến thắng ngắn nhất, ngược lại
     */
	public float getCost(PlayerState currPlayer, PlayerState oppPlayer) {
				
		PawnMove[] currSPT = shortestWinningPath(currPlayer.playerNum());
		PawnMove[] oppSPT = shortestWinningPath(oppPlayer.playerNum());
		
		return currSPT.length - oppSPT.length;
	}
	
	/**
     * Checks which player has more walls left.
     * @param currPlayer player state of current player
     * @param oppPlayer player state of opposite player
     * @return number of walls - positive if current player has more walls,else negative
     */
	public int getWallDiff(PlayerState currPlayer, PlayerState oppPlayer) {
		return currPlayer.wallsLeft() - oppPlayer.wallsLeft();
	}
	
	/**
     * Undo's 2 moves. Calls undo method to undo the previous 2 moves (an entire round).
     * @param playerState 
     * @param otherState
     */
	// default to two moves
	public List<Move> undoMove(PlayerState playerState, PlayerState otherState) {
		return undoMove(2, playerState, otherState);
	}
	
	/**
     * Undo moves. Reverts the board to a previous state before a move or several moves 
     * were made, depending on the number of moves being retracted.
     * @param number number of moves to undo
     * @param playerState
     * @param otherState
     */
	public List<Move> undoMove(int number, PlayerState playerState, PlayerState otherState) {	//Will undo the most previous move. Probably only for Human vs. AI

		List<Move> moves = new LinkedList<Move>();

		for(int i = 0; i < number; ++i) {
			if (moveStack.size() > 2) {
				
				Move last = moveStack.remove(moveStack.size()-1);
				moves.add(last);

				WallMove wm = last.toWallMove();
				if (wm != null) {
					int row = wm.getRow();
					int col = wm.getCol();
					if (wm.getVertical()) {
						rowConnections[row][col] = true;
						rowConnections[row+1][col] = true;
					} else {
						colConnections[row][col] = true;
						colConnections[row][col+1] = true;
					}
					((i % 2 == 0) ? playerState : otherState).undoWall();
				}
			}
		}
		return moves;
	}
	
	/**
     * Save the contents of the quoridor game to a file
     * @param f
     */
	public void save(FileWriter f) {
		try {
			for (int i = 2; i < moveStack.size(); ++i) {
				f.write(moveStack.get(i).toString());
				if (i % 2 == 0) {
					f.write(' ');
				} else {
					f.write('\n');
				}
			}
			f.write(" end");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
     * Returns the current player number. 
     * @return 1 or 2
     */
	public int getCurrentPlayer() {
		return moveStack.size() % 2 == 0 ? 1 : 2;
	}
	
	/**
	 * Returns the total number of moves done.
	 * @return number of moves
	 */
	public int numMoves() {
		return moveStack.size() - 2;
	}
}