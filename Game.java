package Quoridor;

import java.util.List;

/**
 * Represents a game of quoridor.
 */
public class Game {
	private Player player1;
	private Player player2;
	private BoardState board;
	private PlayerState player1state;
	private PlayerState player2state;
	
	/**
     * Create a new game of quoridor.
     * @param player1 Player object representing the first player(left)
     * @param player2 Player object representing the first player(right)
     * @param startState A string of moves representing the starting state of the board
     */
	public Game(Player player1, Player player2, String startState) {				//For creating a new game
		this.player1 = player1;
		this.player2 = player2;
		this.board = new BoardState();
		this.player1state = new PlayerState(1, player1);
		this.player2state = new PlayerState(2, player2);
		MoveStringParser mp = new MoveStringParser(startState); 
		while(mp.movesLeft() > 0) {
			board.applyMove(mp.parseNextMove(), board.getCurrentPlayer() == 1 ? player1state : player2state);
		}
	}
	/**
     * Play a move.
     */
	public void playMove() {
		Player p = (board.getCurrentPlayer() == 1) ? player1 : player2;
		PlayerState ps = (board.getCurrentPlayer() == 1) ? player1state : player2state;
		PlayerState os = (board.getCurrentPlayer() == 1) ? player2state : player1state;
		
		System.out.format("\nChờ người chơi %d (%d tường còn lại)\n", ps.playerNum(), ps.wallsLeft());
		Move m = p.generateMove(board, ps, os);
		
		board.applyMove(m, ps);
	}
	
	/**
     * Determines if a game is over.
     * @return true if the game is over, else false
     */
	public static int playGame(Player player1, Player player2) {
		Game g = new Game(player1, player2, "");
		while(!g.isOver()) {
			g.playMove();
		}
		return g.getWinner();
	}
	
	/**
     * Determines if a game is over.
     * @return true if the game is over, else false
     */
	public boolean isOver() {
		return board.isOver();
	}
	
	/**
     * Determines the winner of the game.
     * @return 1 or 2 to indicate the winning player, or 0 to indicate a game in progress
     */
	public int getWinner() {
		return board.getWinner();
	}
	
	/**
	 * Returns the number of moves done. 
	 * @return number of moves
	 */
	public int numMoves() {
		return board.numMoves();
	}
	
	/**
	 * Prints out the state of the board.
	 */
	public void display() {
		System.out.println();
		board.displayBoard();
	}
	
	/**
	 * Replays a game on the standard output
	 */
	public void replay() {
		List<Move> moves = board.undoMove(100, (board.getCurrentPlayer() == 2) ? player1state : player2state, (board.getCurrentPlayer() == 2) ? player2state : player1state);
		System.out.println();
		System.out.format("    %s vs %s\n", player1state.getPlayerInitString(), player2state.getPlayerInitString());
		System.out.println();
		board.displayBoard();
		for (int i = 0; moves.size() != 0 && !board.isOver(); ++i) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

			PlayerState ps = (board.getCurrentPlayer() == 1) ? player1state : player2state;
			
			System.out.format("Tới lượt Người chơi %d's \n", ps.playerNum());
			
			board.applyMove(moves.remove(moves.size()-1), ps);
			
			board.displayBoard();
		}
		System.out.format("\nNgười chơi %d chiến thắng! (%d bước)", getWinner(), numMoves());
	}
}