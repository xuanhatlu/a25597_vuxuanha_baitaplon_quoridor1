package Quoridor;

/**
 * Interface representing a player.
 *
 */
public interface Player {
	
	/**
	 * Generates the next move.
	 * @param board The current state of the board.
	 * @param playerState The player's state.
	 * @param otherState The other player's state.
	 * @return A valid move to perform.
	 */
	public Move generateMove (BoardState board, PlayerState playerState, PlayerState otherState);
	public String getName();
}