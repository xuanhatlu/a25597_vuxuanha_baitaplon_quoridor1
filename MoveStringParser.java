package Quoridor;

/**
 * Class responsible for breaking apart a move string.
 */
public class MoveStringParser {
	String[] bits;
	int i;
	
	/**
     * Creates a Parser object acting on a given string.
     * @param moveString the string of moves
     */
	public MoveStringParser(String moveString) {
		bits = moveString.split("\\s+");
		if (bits[0].length() == 0) {
			i = 1;
		} else {
			i = 0;
		}
	}
	
	/**
     * Reads the next move from the string and returns it.
     * @return the next move
     */
	public Move parseNextMove() {
		
		assert(i < bits.length);
		if (bits[i].length() > 2) {
			return new WallMove(bits[i++]);
		} else {
			return new PawnMove(bits[i++]);
		}
	}
	
	/**
     * Queries the number of moves left in the string
     * @return the number of moves left
     */
	public int movesLeft() {
		return bits.length - i;
	}
}