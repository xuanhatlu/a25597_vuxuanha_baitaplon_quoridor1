package Quoridor;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class InputReader {

	public static String readWord() {
		StringBuilder sb = new StringBuilder();
		int c = ' ';
		do {
			try {
				c = System.in.read();
			} catch (Exception e) {}
		} while(Character.isWhitespace(c));
		
		while(!Character.isWhitespace(c) && c != -1 && c < 255) {
			sb.append((char)c);
			try {
				c = (char)System.in.read();
			} catch (Exception e) {}
		}
		
		if (c == -1) {
			System.setIn(defaultIn);
			return readWord();
		}
		
		return sb.toString();
	}
	
	public static int readInteger(int low, int high) throws Exception {
		String ln = readWord();
		
		int r = 0;
		try {
			r = Integer.parseInt(ln);
		} catch(Exception e) {
			throw new Exception(String.format("Error: integer expected, not '%s'", ln.trim()));
		}
		
		if (r < low || r > high) {
			throw new Exception(String.format("Error: %d is out of expected range [%d, %d]", r, low, high));
		}
		
		return r;
	}
	
	static InputStream defaultIn = System.in;
	static PrintStream defaultOut = System.out;
	
	private static class NullOutputStream extends OutputStream {
		public void write(int b) {}
	}
	
	public static void stringInput(String s) {
		System.setIn(new ByteArrayInputStream(s.getBytes()));
	}
	public static void fileInput(String s) throws FileNotFoundException {
		System.setIn(new FileInputStream(s));
	}
	
	public static void disableOutput() {
		System.setOut(new PrintStream(new NullOutputStream()));
	}
	public static void enableOutput() {
		System.setOut(defaultOut);
	}
}