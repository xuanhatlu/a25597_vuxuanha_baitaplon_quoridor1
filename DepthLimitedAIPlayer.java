package Quoridor;

	/**
	 * Độ sâu hạn chế Tìm kiếm thông minh bằng AI sử dụng thuật toán tìm kiếm Alpha-Beta NegaMax
	 */
public class DepthLimitedAIPlayer extends AIPlayer {
	final long maxDepth;
	
	/**
	* SmartAI sử dụng một thuật toán tìm kiếm đặc biệt gọi là NegaMax,
        * là 1 phiên bản sửa đổi của thuật toán tìm kiếm Alpha-Beta MiniMax
        * nhìn trước các bước có thể xảy ra trong tương lai để xác định bước di chuyển hưởng lợi tối đa
        * để thực hiện trong một trò chơi. Thuật toán trả về âm vô cùng của đối nghịch
        * người chơi di chuyển để tìm ra những đối thủ di chuyển sẽ có lợi cho hiện tại
        * di chuyển của người chơi. Bằng cách so sánh tất cả những di chuyển tiềm ẩn này, di chuyển tốt nhất có thể
        * được tìm thấy.
	* @param maxDepth
	*/
	public DepthLimitedAIPlayer (int maxDepth) {
		
		if (maxDepth > 0) {
			this.maxDepth = maxDepth;
		} else {
			this.maxDepth = 3;
		}
	}
	
	/**
	* Tính toán di chuyển tốt nhất bằng thuật toán tìm kiếm NegaMax,
        * một loại thuật toán tìm kiếm MiniMax cho phép AI
        * "nhìn trước" một số lượng nhất định di chuyển để xác định
        * di chuyển nó nên. Tìm kiếm này có thể được giới hạn bởi cách xa
        * phía trước nó được phép nhìn.
	* @param BoardState
	* @param PlayerState 
	* @param PlayerState
	* @return bestMove
	*/
	public Move generateMove (BoardState board, PlayerState playerState, PlayerState otherState) {
		
		Move bestMove = null;
		float alpha = Float.NEGATIVE_INFINITY; // giá trị -
		float beta = Float.POSITIVE_INFINITY; // giá trị +
		
		for(Move m : board.allPossibleMoves(playerState)) {
						
			board.applyMove(m, playerState);
			float score = -negaMax(otherState, playerState, board, 1, -beta, -alpha);
			board.undoMove(1, playerState, otherState);
			
			if (score > alpha || alpha == Float.NEGATIVE_INFINITY) {
				alpha = score;
				bestMove = m;
			}
		}
		return bestMove;
	}
	
	/**
	* Việc lặp lại đệ quy thuật toán tìm kiếm NegaMax
        * liên tục nhìn vào những bước di chuyển trong tương lai và đánh giá
        * bước di chuyển tốt nhất mang lại lợi ích tối đa.
	* @param thisPlayer
	* @param otherPlayer
	* @param currState
	* @param currDepth
	* @param alpha
	* @param beta
	* @return alpha
	*/
	public float negaMax(PlayerState thisPlayer, PlayerState otherPlayer, BoardState currState, int currDepth, float alpha, float beta) {
		float score;
		
		if (currState.isOver() || currDepth >= maxDepth) {
			return -currState.getCost(thisPlayer, otherPlayer);
		}
		
		for (Move m : currState.allPossibleMoves(thisPlayer)) {
			
			currState.applyMove(m, thisPlayer);
			score = -negaMax(otherPlayer, thisPlayer, currState, currDepth + 1, -beta, -alpha);
			currState.undoMove(1, thisPlayer, otherPlayer);

			if (score > alpha) {
				alpha = score;
				
				if (alpha >= beta) {
					break;
				}
			}
		}
		return alpha;
	}
		
	/**
        * Trả về những gì tìm kiếm AI được giới hạn bởi;
        * trong trường hợp này là chiều sâu
	* @return Name of AI
	*/
	public String getName() {
		return String.format("smart-ai depth %d", maxDepth); 
	}
}