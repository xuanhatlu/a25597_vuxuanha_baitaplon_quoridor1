package Quoridor;

import java.io.FileNotFoundException;

public class UserInterface {
	
	static public void main(String[] args) {
		
		Game replayGame = null;
		
		while(true) {
		
			int menuAction = 0;
			
			System.out.println();
			System.out.println("    1. Tạo Game");
			System.out.println("    2. Thoát");
			
			try {
				String command = InputReader.readWord();
				
				if (command.toLowerCase().equals("new") || command.toLowerCase().equals("1")) {
					menuAction = 1;
				}else if (command.toLowerCase().equals("quit") || command.toLowerCase().equals("2")) {
					menuAction = 2;
				} else {
					System.err.format("Lỗi: Lệnh không hợp lệ '%s'\n", command);
					return;
				}
				
			} catch (Exception e) {
				System.err.println(e.getMessage());
				return;
			}
			
			Player player1 = null;
			Player player2 = null;
			String startMoves = null;
			
			switch (menuAction)
			{
			case 1: // New game
				player1 = choosePlayer(1);
				if (player1 == null) {
					return;
				}
				
				player2 = choosePlayer(2);
				if (player2 == null) {
					return;
				}
				
				startMoves = "";
				
				break;
				
			case 2: // Quit
				return;
			}
			
			// Play the game
			
			Game game = new Game(player1, player2, startMoves);
			while(!game.isOver()) {
                                game.display();
				game.playMove();
                                //game.display();
			}
			
			game.display();
			System.out.format("\nNgười chơi %d chiến thắng! (%d bước)", game.getWinner(), game.numMoves());
			
			replayGame = game;
		}
	}
	
	static Player choosePlayer(int playerNum) {
		System.out.format("\nNhập người chơi %d ([n]gười, [m]áy): ", playerNum);
		
		String type = null;
		try {
			type = InputReader.readWord();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return null;
		}
		
		Player p = null;
		
		if (type.toLowerCase().equals("n") || type.toLowerCase().equals("human") || type.toLowerCase().equals("h")) {
			p = new HumanPlayer();
		}else if (type.toLowerCase().equals("m") || type.toLowerCase().equals("ai") || type.toLowerCase().equals("smart")) {		
			type = "depth";
			if (type.toLowerCase().equals("depth")) {
				System.out.format("\nNhập độ sâu tối đa: ");
				try {
					p = new DepthLimitedAIPlayer(InputReader.readInteger(3, 10));
				} catch (Exception e) {
					System.err.println(e.getMessage());
					return null;
				}
			}else {
				p = new DepthLimitedAIPlayer(3);
			}
		} else {
			System.err.format("Chọn loại người chơi không hợp lệ '%s'\n", type);
			return null;
		}
		
		return p;
	}
}