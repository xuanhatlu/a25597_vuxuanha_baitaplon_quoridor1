package Quoridor;

/**
 * An abstract base class for all moves.
 */
public abstract class Move {
	int row;
	int col;

	protected Move(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	public final PawnMove toPawnMove() {
		if (this instanceof PawnMove) {
			return (PawnMove)this;
		} else {
			return null;
		}
	}
	public final WallMove toWallMove() {
		if (this instanceof WallMove) {
			return (WallMove)this;
		} else {
			return null;
		}
	}
	
	/**
     * Returns the row number of the move.
     * @return the row number 0-8
     */
	public int getRow() {
		return row;
	}
	
	/**
     * Returns the column number of the move.
     * @return the column number 0-8
     */
	public int getCol() {
		return col;
	}
}