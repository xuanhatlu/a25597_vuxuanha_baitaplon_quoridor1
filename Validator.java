package Quoridor;

import java.util.List;

public class Validator {

	/**
	 * Construct an instance of the Validator class
	 */
	public Validator() {
	}
        /**
            * Kiểm tra tính hợp lệ của một chuỗi di chuyển theo trình tự.
            * Trình tự này có giá trị khi và chỉ khi mỗi (không gian cách nhau)
            * di chuyển trong danh sách là hợp lệ,
            * bắt đầu từ vị trí ban đầu của trò chơi.
            * Khi trận đấu đã thắng, không có động thái nào khác là hợp lệ.
            * @param move là một danh sách các di chuyển kế tiếp
            * @return đúng nếu danh sách không có động thái không hợp lệ
        */
	public boolean check(String moves) {
		BoardState state = new BoardState();
		MoveStringParser mp = new MoveStringParser(moves);
		PlayerState ps1 = new PlayerState(1, null);
		PlayerState ps2 = new PlayerState(2, null);
		
		while(mp.movesLeft() != 0) {
			Move m = mp.parseNextMove();
			
			if (state.isMoveValid(m, ps1)) {
				state.applyMove(m, ps1);
			} else {
				return false;
			}
			
			PlayerState t = ps1;
			ps1 = ps2;
			ps2 = t;
		}
		
		return true;
	}

	public boolean check(List<String> moves) {
		BoardState state = new BoardState();
		PlayerState ps1 = new PlayerState(1, null);
		PlayerState ps2 = new PlayerState(2, null);
		
		Move m = null;
		
		for(String ms : moves) {
			
			if (m != null) {
				state.applyMove(m, ps1);
				
				PlayerState t = ps1;
				ps1 = ps2;
				ps2 = t;
			}
			
			if (ms.length() > 2) {
				m = new WallMove(ms);
			} else {
				m = new PawnMove(ms);
			}
		}
		
		return state.isMoveValid(m, ps1);
	}
}
