package Quoridor;

/**
 * Represends a Move that places a wall.
 *
 */
public class WallMove extends Move {

	boolean vertical;

	public WallMove(String moveString) {
		super(Character.toLowerCase(moveString.charAt(1)) - '1',
			  Character.toLowerCase(moveString.charAt(0)) - 'a');
		vertical = Character.toLowerCase(moveString.charAt(2)) == 'v';
	}
	
	public WallMove(int row, int col, boolean vertical) {
		super(row, col);
		this.vertical = vertical;
	}
	
	public String toString() {
		return String.format("%c%c%c", col+'a', row+'1', vertical ? 'v' : 'h');
	}
	
	public boolean getVertical() {
		return vertical;
	}
}