package Quoridor;

/**
  * Tóm tắt: lớp cơ sở cho tất cả các lớp người chơi.
  * Các lớp dẫn xuất phải thực hiện ghi đè phương thức generateMove (tạo bước đi)
 */
public abstract class AbstractPlayer implements Player {
	
	protected AbstractPlayer() {
		
	}
}