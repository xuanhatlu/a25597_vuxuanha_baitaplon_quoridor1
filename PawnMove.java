 package Quoridor;

 /**
  * Represents a pawn move.
  */
public class PawnMove extends Move {
	
	/**
     * Creates a move object.
     * @param moveString Two character string representing the move eg. a7
     */
	public PawnMove(String moveString) {
		super(Character.toLowerCase(moveString.charAt(1)) - '1',
			  Character.toLowerCase(moveString.charAt(0)) - 'a');
	}
	
	/**
     * Creates a move object from the row and column number
     * @param row row number of the move
     * @param col column number of the move
     */
	public PawnMove(int row, int col) {
		super(row, col);
	}
	
	/**
     * Compares a PawnMove object to another
     * @param other the other PawnMove object
     * @return true if they are equal, else false
     */
	public boolean equals(PawnMove other) {
		return this.row == other.row && this.col == other.col;
	}
	
	/**
     * Moves the pawn to the left.
     * @return new position of the pawn
     */
	public PawnMove left() {
		return new PawnMove(row, col - 1);
	}
	
	/**
     * Moves the pawn to the right.
     * @return new position of the pawn
     */
	public PawnMove right() {
		return new PawnMove(row, col + 1);
	}
	
	/**
     * Moves the pawn up.
     * @return new position of the pawn
     */
	public PawnMove top() {
		return new PawnMove(row - 1, col);
	}
	
	/**
     * Moves the pawn down.
     * @return new position of the pawn
     */
	public PawnMove bottom() {
		return new PawnMove(row + 1, col);
	}
	
	/**
     * Leaps the pawn over the other pawn.
     * @param op opponent's pawn position 
     * @return new position of the pawn
     */
	public PawnMove leap(PawnMove op) {
		return new PawnMove(row + (op.row - row) * 2, col + (op.col - col) * 2);
	}
	
	/**
     * Leaps diagonally left over the other pawn.
     * @param op opponent's pawn position
     * @return new position of the pawn
     */
	public PawnMove leapLeft(PawnMove op) {
		int Ax = col;
		int Ay = row;
		int Bx = op.col;
		int By = op.row;
		return new PawnMove(By+Ax-Bx, Bx-Ay+By);
	}
	
	/**
     * Leaps diagonally right over the other pawn.
     * @param op opponent's pawn position
     * @return new position of the pawn
     */
	public PawnMove leapRight(PawnMove op) {
		int Ax = col;
		int Ay = row;
		int Bx = op.col;
		int By = op.row;
		return new PawnMove(By-Ax+Bx, Bx+Ay-By);
	}
	
	/**
     * Checks whether the pawn is inside the board or not.
     * @return true if pawn is inside the board, else false
     */
	public boolean insideBoard() {
		return row >= 0 && row < 9 && col >= 0 && col < 9;
	}

	@Override
	public String toString() {
		return String.format("%c%c", 'a'+col, '1'+row);
	}
}