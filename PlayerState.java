package Quoridor;

	/**
	 * Stores and controls the number of walls a player has remaining
	 */
public class PlayerState {
	private int walls;
	private int playerNum;
	private Player player;
	
	/**
	 * Constructs a PlayerState with an initial number of walls
	 * @param playerNum
	 * @param player
	 */
	public PlayerState(int playerNum, Player player) {
		this.walls = 10;
		this.playerNum = playerNum;
		this.player = player;
	}
	
	/**
	 * Constructs a PlayerState from saved games
	 * @param playerNum
	 * @param numWalls
	 * @param player
	 */
	private PlayerState(int playerNum, int numWalls, Player player) {
		this.walls = numWalls;
		this.playerNum = playerNum;
		this.player = player;
	}
	
	/**
	 * Decrement number of walls when a wall is placed on the board
	 */
	public void useWall() {
		assert(walls != 0);
		walls--;
	}
	
	/**
	 * Returns the number of walls a player has remaining
	 * @return walls
	 */
	public int wallsLeft() {
		return walls;
	}
	
	/**
	 * When a wall move is undone, increment number of walls that player has
	 */
	public void undoWall() {
		walls++;
	}
	
	/**
	 * Return the player number.
	 * @return playerNum
	 */
	public int playerNum() {
		return playerNum;
	}
	
	/**
	 * Create a copy of a PlayerState
	 * @return new PlayerState
	 */
	public PlayerState clone() {
		return new PlayerState(playerNum, walls, player);
	}
	
	/**
	 * Return the type of player (human or AI)
	 * @return Type of Player
	 */
	public String getPlayerInitString() {
		return player.getName();
	}
}